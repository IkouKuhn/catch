<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/show/{id}', 'HomeController@show');
Route::post('/update/{id}', 'HomeController@update');
Route::get('/delete/{id}', 'HomeController@destroy');
Route::get('/exportcsv', 'HomeController@exportCsv');
Route::get('/exportxls', 'HomeController@exportXls');
Route::get('/exportxlsx', 'HomeController@exportXlsx');
Route::get('/exportjson', 'HomeController@exportJson');
Route::get('/exportpdf', 'HomeController@exportPdf');
