@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Orders</h3>
    <div class="row">
        <div class="col-md-12">
            <a href="{{ URL::to('exportcsv/') }}" class="btn btn-primary">Export CSV</a>
            <a href="{{ URL::to('exportxls/') }}" class="btn btn-primary">Export XLS</a>
            <a href="{{ URL::to('exportxlsx/') }}" class="btn btn-primary">Export XLSX</a>
            <a href="{{ URL::to('exportjson/') }}" class="btn btn-primary">Export JSON</a>
            <a href="{{ URL::to('exportpdf/') }}" class="btn btn-primary">Export PDF</a>
        </div>
    </div>


    <div class="row">
        <div class="col-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>order_id</th>
                        <th>order_datetime</th>
                        <th>total_order_value</th>
                        <th>average_unit_price</th>
                        <th>distinct_unit_count</th>
                        <th>total_units_count</th>
                        <th>customer_state</th>
                        <th>actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                    @if($order->total_order_value > 0)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->order_datetime }}</td>
                        <td>{{ $order->total_order_value }}</td>
                        <td>{{ $order->average_unit_price }}</td>
                        <td>{{ $order->distinct_unit_count }}</td>
                        <td>{{ $order->total_units_count }}</td>
                        <td>{{ $order->customer_state }}</td>
                        <td>
                            <a href="{{ URL::to('show/'.$order->id.'?page='.$orders->currentPage()) }}" class="btn btn-info btn-xs"><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ URL::to('delete/'.$order->id) }}" onclick= "return confirm('Are you sure ?')" class="btn btn-danger btn-xs"><i
                                    class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
            <div class="pages">{!! str_replace('/?', '?', $orders->appends([])->render()) !!}</div>
        </div>
    </div>
</div>
@endsection