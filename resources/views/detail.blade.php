@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Order ID : {{$order->id}}</h3>
    <div class="row">

    </div>

    <h4>Summary</h4>
    <form action="{{ URL::to('update/'.$order->id) }}" method="POST">
        @csrf
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="order_date">Order Date</label>
                    <input type="text" name="order_date" class="form-control datetimepicker-input" id="order_date"
                        data-toggle="datetimepicker" data-target="#order_date" />
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Total Order Value</label>
                    <input type="text" class="form-control" value="{{$order->total_order_value}}" readonly>
                    <input type="hidden" class="form-control" name="page" value="{{$order->page}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Average Unit Price</label>
                    <input type="text" class="form-control" value="{{$order->average_unit_price}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Distinct Unit Count</label>
                    <input type="text" class="form-control" value="{{$order->distinct_unit_count}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Total Unit Count</label>
                    <input type="text" class="form-control" value="{{$order->total_units_count}}" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Customer Name</label>
                    <input type="text" name="first_name" class="form-control" value="{{$order->customer->first_name}}">
                    <input type="text" name="last_name" class="form-control" value="{{$order->customer->last_name}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
                    <input type="email" name="email" class="form-control" value="{{$order->customer->email}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Phone</label>
                    <input type="text" name="phone" class="form-control" value="{{$order->customer->phone}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Shipping Price</label>
                    <input type="text" name="shipping_price" class="form-control" value="{{$order->shipping_price}}">
                </div>
            </div>
        </div>
        <h4>Shipping Address</h4>
        <div class="row col-md-12">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleInputPassword1">Street</label>
                    <input type="text" name="street" class="form-control"
                        value="{{$order->customer->shipping_address->street}}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputPassword1">Postal Code</label>
                    <input type="text" name="postal_code" class="form-control"
                        value="{{$order->customer->shipping_address->postcode}}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputPassword1">Suburb</label>
                    <input type="text" name="suburb" class="form-control"
                        value="{{$order->customer->shipping_address->suburb}}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputPassword1">State</label>
                    <input type="text" name="state" class="form-control"
                        value="{{$order->customer->shipping_address->state}}">
                </div>
            </div>
            <div class="col-md-12">
                <h4>Location</h4>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Latitude</label>
                    <input type="text" name="latitude" class="form-control" id="latitude"
                        value="{{$order->customer->shipping_address->latitude}}" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Longitude</label>
                    <input type="text" name="longitude" class="form-control" id="longitude"
                        value="{{$order->customer->shipping_address->longitude}}" readonly>
                </div>
            </div>
            <div class="col-md-12">
                <div name="googleMap" id="googleMap" style="width:100%;height:400px;"></div>
            </div>
        </div>
        <br>
        <h4>Items</h4>
        @foreach($order->items as $i=>$item)
        <div class="row col-md-12">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleInputPassword1">Product Title</label>
                    <input type="text" name="item[{{$i}}][title]" class="form-control" value="{{$item->product->title}}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleInputPassword1">Product Subtitle</label>
                    <input type="text" name="item[{{$i}}][subtitle]" class="form-control" value="{{$item->product->subtitle}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Image</label>
                    <input type="text" name="item[{{$i}}][image]" class="form-control" value="{{$item->product->image}}">
                    <img src="{{$item->product->image}}" width="100%;">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Thumbnail</label>
                    <input type="text" name="item[{{$i}}][thumbnail]" class="form-control" value="{{$item->product->thumbnail}}">
                    <img src="{{$item->product->image}}" width="100%;">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Brand</label>
                    <select name="item[{{$i}}][brand]" class="form-control">
                        @foreach($brands as $brand)
                        <option value="{{$brand->id}}" {{$brand->id == $item->product->brand_id ? "selected" : ""}}>
                            {{$brand->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleInputPassword1">URL</label>
                    <input type="text" name="item[{{$i}}][url]" class="form-control" value="{{$item->product->url}}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleInputPassword1">UPC</label>
                    <input type="text" name="item[{{$i}}][upc]" class="form-control" value="{{$item->product->upc}}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleInputPassword1">gtin14</label>
                    <input type="text" name="item[{{$i}}][gtin14]" class="form-control" value="{{$item->product->gtin14}}">
                </div>
            </div>
        </div>
        @endforeach
        <div class="row col-md-12">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Discounts</label>
                    <select multiple name="discounts[]" class="form-control">
                        @foreach($discounts as $i=>$discount)
                        <option value="{{$discount->id}}"
                            {{in_array($discount->id,$selected_discounts)? "selected" : ""}}>
                            {{$discount->type." - ".$discount->value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-12">
                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{ URL::to('/?page=1') }}" class="btn btn-danger">Cancel</a>
                </div>
            </div>
        </div>
    </form>


</div>

</div>
</div>
@endsection
@push('scripts')
<script>
    $(function () {
        $('#order_date').datetimepicker({
            defaultDate: "{{$order->order_date}}",
        });
    });
</script>
<script type="text/javascript">
    function myMap() {
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var myCenter = new google.maps.LatLng(latitude,longitude);
        var mapProp= {
            center:myCenter,
            zoom:15,
        };
        
        var map = new google.maps.Map(document.getElementById('googleMap'),mapProp);
        var marker = new google.maps.Marker({position: myCenter, map:map});

        map.addListener('click', function(e) {
            changeMarkerPosition(e.latLng, marker, map);
        });
    }
    function changeMarkerPosition(latLng, marker, map) {
        marker.setPosition(latLng);
        $('#latitude').val(latLng.lat());
        $('#longitude').val(latLng.lng());
        map.panTo(latLng);
    }
</script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBeJ5D7DJDeULmbKsM1GJ21imtMcXXwCYU&callback=myMap"></script>
@endpush