<table>
    <thead>
    <tr>
        <th>order_id</th>
        <th>order_datetime</th>
        <th>total_order_value</th>
        <th>average_unit_price</th>
        <th>distinct_unit_count</th>
        <th>total_units_count</th>
        <th>customer_state</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        @if($order->total_order_value > 0)
        <tr>
            <td>{{ $order->id }}</td>
            <td>{{ $order->order_datetime }}</td>
            <td>{{ $order->total_order_value }}</td>
            <td>{{ $order->average_unit_price }}</td>
            <td>{{ $order->distinct_unit_count }}</td>
            <td>{{ $order->total_units_count }}</td>
            <td>{{ $order->customer_state }}</td>
        </tr>
        @endif
    @endforeach
    </tbody>
</table>