
## Catch Coding Challenge - PHP

Contact Details:

* Name : Ricko Dwi Anggriawan

* Email : ikou_kuhn@hotmail.com

* LinkedIn : https://www.linkedin.com/in/ricko-dwi-anggriawan-8a5b7542/


## Setup/Bootstrapping

* Clone or download the app from this repository.

* Create a new database in your database. 

Example:
```
CREATE SCHEMA `catch` ;
```

* Setup your database by creating .env file located in the root folder. You can copy the template from .env.example file. Here's the example, I'm using mysql with "catch" as the database name:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=catch
DB_USERNAME=root
DB_PASSWORD=admin123
```

* Make sure composer & artisan is already installed in your device, then run these commands from the root folder on terminal/command prompt. If the migrate command is failed, make sure your database configuration is correct.

```
composer update
```
```
php artisan migrate
```
```
php artisan key:generate
```
* Setup is done, you can deploy this app or run it locally by running `php artisan serve` on the terminal, then access it on localhost:8000

* Note that it may take a while to load when running the app on the first time, because the code is importing data from the jsonl file to the database.

## REST API Usage
I've created REST API for the orders that has been imported. Here's the available API methods:
```
GET <host-name>/api/orders
POST <host-name>/api/orders/
PUT <host-name>/api/orders/{order_id}
DELETE <host-name>/api/orders/{order_id}
```

### `GET <host-name>/api/orders`

* This method is for getting all of the orders, you can paginate the list by using the `?page` parameter.

* Example : `GET http://localhost:8000/api/orders?page=1`

* The default items displayed per page is 20. You can customize it by adding the `?per_page` parameter.

* Example: `GET http://localhost:8000/api/orders?page=1&per_page=10`


### `POST <host-name>/api/orders/`

* This method will create a new order record.

* Example: `POST http://localhost:8000/api/orders?order_date=2019-11-19 08:21:40&customer_id=7126333&shipping_price=6.66&products[0][product_id]=3879592&products[0][price]=2.9&products[0][quantity]=2&discounts[0][type]=DOLLAR&discounts[0][value]=1&discounts[0][priority]=1`

* "products" & "discounts" are stored in array parameter. Discount are not mandatory.



### `PUT <host-name>/api/orders/{order_id}`

* This method will update the existing order based on `{order_id}` parameter.

* Example : `PUT http://localhost:8000/api/orders/1125?order_date=2019-11-19 08:21:40&customer_id=7126333&shipping_price=6.66&products[0][product_id]=3879592&products[0][price]=2.9&products[0][quantity]=2&discounts[0][type]=DOLLAR&discounts[0][value]=1&discounts[0][priority]=1&type=replace` 

* "products" & "discounts" are stored in array parameter. Discount are not mandatory. If you add "type=replace" parameter, it will replace all of the existing product with the new inputs from API.



### `DELETE <host-name>/api/orders/{order_id}`

* This will delete the order record based on `{order_id}` parameter.

* Example : `DELETE http://localhost:8000/api/orders/1126`



## Coding Explanations/FAQ

* Q : Why are you inserting it to database? 

* A : Storing it in database, make it easier to maintain & access for multiple purposes.

Here's the ER Diagram of the database:
![http://drive.google.com/uc?export=view&id=1b6lwyzKHX7d2tmjvrqDaE_8CY02v8zf-](http://drive.google.com/uc?export=view&id=1b6lwyzKHX7d2tmjvrqDaE_8CY02v8zf-)

* Q : What architecture format do you use?
* A : I'm using standard MVC format on Laravel.

* Q : Why are there static function in Order Model?
* A : I've created it so that it can be accessed from multiple controllers,exports, or other librarires. It's also easier to maintain that way.

* Q : What external libraries do you use?
* A : Here are some external library that I use on this app:
    * rodenastyle/stream-parser for streaming the input jsonl files.
    * maatwebsite/excel for exporting data to csv,xls & xlsx format.
    * barryvdh/laravel-dompdf for exporting data to pdf format.
    * Google Maps API for geocoding & map data.

