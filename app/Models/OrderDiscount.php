<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDiscount extends Model
{
    protected $table = "order_discounts";
    protected $guarded = ['id'];
    
    public function order()
	{
        return $this->belongsTo('App\Models\Order');
    }

    public function discount()
	{
        return $this->belongsTo('App\Models\Discount');
    }
}
