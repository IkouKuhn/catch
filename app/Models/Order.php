<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";
    protected $guarded = ['id'];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Item');
    }

    public function order_discounts()
    {
        return $this->hasMany('App\Models\OrderDiscount')->orderBy('priority', 'ASC');
    }

    public static function summary($paginate = false, $per_page = 20)
    {
        if (is_numeric($paginate)) {
            $orders = Order::where('id', '=', $paginate)->get();
        } else if ($paginate == true) {
            $orders = Order::paginate($per_page);
        } else {
            $orders = Order::all();
        }
        foreach ($orders as $i => $order) {
            $orders[$i]->order_datetime = date('Y-m-d\TH:i:s.Z\Z', strtotime($order->order_date));
            $total_price = 0;
            $orders[$i]->average_unit_price = 0;
            $orders[$i]->distinct_unit_count = 0;
            $orders[$i]->total_units_count = 0;
            if (isset($order->items)) {
                $orders[$i]->average_unit_price = number_format($order->items->avg('unit_price'),2);
                foreach ($order->items as $item) {
                    $total_price += $item->unit_price * $item->quantity;
                    $orders[$i]->distinct_unit_count++;
                    $orders[$i]->total_units_count += $item->quantity;
                    if (isset($item->product)) {
                        $item->product;
                        if (isset($item->product->brand)) {
                            $item->product->brand;
                        }

                    }
                }
            }
            if (isset($order->order_discounts)) {
                foreach ($order->order_discounts as $order_discount) {
                    if (isset($order_discount->discount) && $order_discount->discount->type == "DOLLAR") {
                        $total_price = $total_price - $order_discount->discount->value;
                    } else if (isset($order_discount->discount) && $order_discount->discount->type == "PERCENTAGE") {
                        $total_price = $total_price - ($total_price * ($order_discount->discount->value / 100));
                    }

                }
            }
            $orders[$i]->total_order_value = number_format($total_price,2);
            $orders[$i]->customer_state = isset($order->customer->shipping_address) ? $order->customer->shipping_address->state : "";

        }

        return $orders;
    }
}
