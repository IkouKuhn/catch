<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Discount;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderDiscount;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ShippingAddress;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Rodenastyle\StreamParser\StreamParser;
use Tightenco\Collect\Support\Collection;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function validation($data)
    {
        return \Validator::make($data->all(), [
            'order_date' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'shipping_price' => 'required',
            'street' => 'required',
            'postal_code' => 'required',
            'suburb' => 'required',
            'state' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'item.*.title' => 'required',
            'item.*.image' => 'required',
            'item.*.thumbnail' => 'required',
            'item.*.brand' => 'required',
            'item.*.url' => 'required',
        ]);
    }

    public function insertDb($order)
    {
        $ordercheck = Order::find($order['order_id']);
        if (!isset($ordercheck->id)) {
            $orderdb = new Order;
            $orderdb->id = $order['order_id'];
            $orderdb->order_date = date('Y-m-d H:i:s', strtotime($order['order_date']));
            $orderdb->shipping_price = $order['shipping_price'];
            $orderdb->save();

            if (isset($order['customer'])) {
                $customercheck = Customer::find($order['customer']['customer_id']);
                if (!isset($customercheck->id)) {
                    $customer = $this->insertCustomer($order['customer']);

                    if (isset($order['customer']['shipping_address'])) {
                        $shippingcheck = ShippingAddress::where('customer_id', '=', $customer->id)->first();
                        if (!isset($shippingcheck->id)) {
                            $shipping = $this->insertShipping($customer->id, $order['customer']['shipping_address']);
                        }
                    }
                }
                $orderdb->customer_id = $customer->id;
                $orderdb->save();
            }

            if (isset($order['items'])) {
                foreach ($order['items'] as $item) {
                    $item_order = $this->insertItem($order['order_id'], $item);
                }
            }
            if (isset($order['discounts'])) {
                foreach ($order['discounts'] as $discount) {
                    $disc = $this->insertDiscount($order['order_id'], $discount);
                }
            }

        }
    }

    public function insertDiscount($order_id, $item)
    {
        $discount = Discount::where('type', '=', $item['type'])->where('value', '=', $item['value'])->first();
        if (!isset($discount->id)) {
            $discount = new Discount;
            $discount->type = $item['type'];
            $discount->value = $item['value'];
            $discount->save();
        }
        $order_disc = OrderDiscount::where('order_id', '=', $order_id)->where('discount_id', '=', $discount->id)->first();
        if (!isset($order_disc)) {
            $order_disc = new OrderDiscount;
            $order_disc->order_id = $order_id;
            $order_disc->discount_id = $discount->id;
            $order_disc->priority = $item['priority'];
            $order_disc->save();
        }

        return $discount->id;
    }

    public function insertItem($order_id, $item)
    {
        $itemdb = new Item;
        $itemdb->order_id = $order_id;
        $itemdb->quantity = $item['quantity'];
        $itemdb->unit_price = $item['unit_price'];
        $itemdb->save();

        if (isset($item['product'])) {
            $product = Product::find($item['product']['product_id']);
            if (!isset($product->id)) {
                $product = $this->insertProduct($item['product']);
            }

            $itemdb->product_id = $product->id;
            $itemdb->save();
        }

        return $itemdb;
    }

    public function insertCustomer($input)
    {
        $customer = new Customer;
        $customer->id = $input['customer_id'];
        $customer->first_name = $input['first_name'];
        $customer->last_name = $input['last_name'];
        $customer->email = $input['email'];
        $customer->phone = $input['phone'];
        $customer->save();

        return $customer;
    }

    public function insertShipping($customer_id, $input)
    {
        $shipping = new ShippingAddress;
        $shipping->customer_id = $customer_id;
        $shipping->street = $input['street'];
        $shipping->postcode = $input['postcode'];
        $shipping->suburb = $input['suburb'];
        $shipping->state = $input['state'];

        // get latitude & longitude from google maps API
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address="
        . urlencode($shipping->street . ',' . $shipping->suburb . ',' . $shipping->state) .
            "&key=AIzaSyBeJ5D7DJDeULmbKsM1GJ21imtMcXXwCYU";
        StreamParser::json($url)->each(function (Collection $gmap) use ($shipping) {
            if (isset($gmap['results'][0])) {
                $shipping->latitude = $gmap['results'][0]['geometry']['location']['lat'];
                $shipping->longitude = $gmap['results'][0]['geometry']['location']['lng'];
            }
        });

        $shipping->save();

        return $shipping;
    }

    public function insertProduct($item)
    {
        $product = new Product;
        $product->id = $item['product_id'];
        $product->title = $item['title'];
        $product->subtitle = $item['subtitle'];
        $product->image = $item['image'];
        $product->thumbnail = $item['thumbnail'];
        $product->url = $item['url'];
        $product->upc = $item['upc'];
        $product->gtin14 = $item['gtin14'];
        $product->created_at = $item['created_at'];
        $product->save();

        if (isset($item['category'])) {
            foreach ($item['category'] as $cat) {
                $category = Category::where('title', '=', $cat)->first();
                // create new category if doesn't exist before
                if (!isset($category->id)) {
                    $category = new Category;
                    $category->title = $cat;
                    $category->save();
                }
                $checkprodcat = ProductCategory::where('product_id', '=', $product->id)->where('category_id', '=', $cat)->first();
                if (!isset($checkprodcat->id)) {
                    $product_category = new ProductCategory;
                    $product_category->product_id = $product->id;
                    $product_category->category_id = $category->id;
                    $product_category->save();
                }

            }
        }
        if (isset($item['brand'])) {
            $brand = Brand::find($item['brand']['id']);
            // create new brand if doesn't exist before
            if (!isset($brand->id)) {
                $brand = new Brand;
                $brand->id = $item['brand']['id'];
                $brand->name = $item['brand']['name'];
                $brand->save();
            }
            $product->brand_id = $brand->id;
            $product->save();
        }

        return $product;
    }
}
