<?php

namespace App\Http\Controllers;

use App\Exports\OrdersExport;
use App\Models\Brand;
use App\Models\Discount;
use App\Models\Order;
use App\Models\OrderDiscount;
use File;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Response;
use Rodenastyle\StreamParser\StreamParser;
use Tightenco\Collect\Support\Collection;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!isset($request->page)) {
            $this->insertInputs();
        }

        $orders = Order::summary(true);

        return view('home', compact('orders'));

    }

    private function insertInputs()
    {
        $url = "https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl";
        StreamParser::json($url)->each(function (Collection $order) {
            $this->insertDb($order);
        });
    }

    public function exportCsv()
    {
        return Excel::download(new OrdersExport, 'out.csv', \Maatwebsite\Excel\Excel::CSV, ['Content-Type' => 'text/csv']);
    }

    public function exportXls()
    {
        return Excel::download(new OrdersExport, 'out.xls', \Maatwebsite\Excel\Excel::XLS);
    }

    public function exportXlsx()
    {
        return Excel::download(new OrdersExport, 'out.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function exportJson()
    {
        $orders = Order::summary();
        $json_data = "";
        foreach ($orders as $i => $order) {
            if ($order->total_order_value > 0) {
                $json_data .= '{"order_id":' . $order->order_id . ", ";
                $json_data .= '"order_datetime":' . $order->order_datetime . ", ";
                $json_data .= '"total_order_value":' . $order->total_order_value . ", ";
                $json_data .= '"average_unit_price":' . $order->average_unit_price . ", ";
                $json_data .= '"distinct_unit_count":' . $order->distinct_unit_count . ", ";
                $json_data .= '"total_units_count":' . $order->total_units_count . ", ";
                $json_data .= '"customer_state":' . $order->customer_state . "}\r\n";
            }
        }
        File::put(public_path('out.jsonl'), $json_data);
        return Response::download(public_path('out.jsonl'));
    }

    public function exportPdf()
    {
        $orders = Order::summary();
        $pdf = PDF::loadView('export', compact('orders'))->setPaper('a4', 'landscape');
        return $pdf->download('out.pdf');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $order = Order::summary($id);
        $order = $order[0];
        $brands = Brand::all();
        $discounts = Discount::orderBy('type', 'ASC')->orderBy('value', 'ASC')->get();
        $selected_discounts = [];
        foreach ($order->order_discounts as $order_discount) {
            $selected_discounts[] = $order_discount->discount_id;
        }
        if (isset($request->page)) {
            $order->page = $request->page;
        } else {
            $order->page = 1;
        }

        return view('detail', compact('order', 'brands', 'discounts', 'selected_discounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validation($request);
        if ($validator->fails()) {
            return redirect('/show/' . $id)->with('danger', 'Data failed to save.')->withErrors($validator);
        }

        $order = Order::find($id);
        $order->order_date = date('Y-m-d H:i:s', strtotime($request->order_date));
        $order->shipping_price = $request->shipping_price;
        $order->save();
        if (isset($order->customer)) {
            $order->customer->first_name = $request->first_name;
            $order->customer->last_name = $request->last_name;
            $order->customer->email = $request->email;
            $order->customer->phone = $request->phone;
            $order->customer->save();
            if (isset($order->customer->shipping_address)) {
                $order->customer->shipping_address->street = $request->street;
                $order->customer->shipping_address->postcode = $request->postal_code;
                $order->customer->shipping_address->suburb = $request->suburb;
                $order->customer->shipping_address->state = $request->state;
                $order->customer->shipping_address->latitude = $request->latitude;
                $order->customer->shipping_address->longitude = $request->longitude;
                $order->customer->shipping_address->save();
            }
            if (isset($order->items)) {
                foreach ($order->items as $i => $item) {
                    $order->items[$i]->product->title = $request->item[$i]['title'];
                    $order->items[$i]->product->subtitle = $request->item[$i]['subtitle'];
                    $order->items[$i]->product->image = $request->item[$i]['image'];
                    $order->items[$i]->product->thumbnail = $request->item[$i]['thumbnail'];
                    $order->items[$i]->product->brand_id = $request->item[$i]['brand'];
                    $order->items[$i]->product->url = $request->item[$i]['url'];
                    $order->items[$i]->product->upc = $request->item[$i]['upc'];
                    $order->items[$i]->product->gtin14 = $request->item[$i]['gtin14'];
                    $order->items[$i]->product->save();
                }
            }
            OrderDiscount::where('order_id', '=', $order->id)->delete();
            if (isset($request->discounts)) {
                foreach ($request->discounts as $discount) {
                    $order_discount = new OrderDiscount;
                    $order_discount->order_id = $order->id;
                    $order_discount->discount_id = $discount;
                    $order_discount->save();
                }
            }

        }

        return redirect('/?page=' . $request->page)->with('success', 'Data has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        if (isset($order->id)) {
            $order->delete();
        }

        return redirect('/?page=1')->with('success', 'Data has been deleted.');
    }
}
