<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Discount;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderDiscount;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate = false;
        $per_page = 20;
        if (isset($request->page)) {
            $paginate = true;
        }
        
        if (isset($request->per_page)) {
            $per_page = $request->per_page;
        }

        if (isset($request->page)) {
            $orders = Order::summary($paginate, $per_page);
        } else {
            $orders = Order::summary();
        }

        return $orders;

    }

    public function create(Request $request)
    {
        $order = new Order;
        if (isset($request->order_date)) {
            $order->order_date = date('Y-m-d H:i:s', strtotime($request->order_date));
        }

        if (isset($request->customer_id)) {
            $check = Customer::find($request->customer_id);
            if (isset($check->id)) {
                $order->customer_id = $request->customer_id;
            } else {
                return ['message' => 'Customer ID does not exist'];
            }

        }
        if (isset($request->shipping_price)) {
            $order->shipping_price = $request->shipping_price;
        }

        $order->save();

        // insert items
        if (isset($request->products)) {
            foreach ($request->products as $i => $product) {
                $item = $this->insertItemApi($product, $order);
                if (isset($item['message'])) {
                    return $item;
                }

            }
        }
        // insert discount
        if (isset($request->discounts)) {
            foreach ($request->discounts as $discount) {
                $disc = $this->insertDiscountApi($discount, $order);
                if (isset($disc['message'])) {
                    return $disc;
                }

            }
        }

        return ['message' => 'Saved Successfully.', 'data' => Order::summary($order->id)];
    }

    private function insertItemApi($product, $order)
    {
        if (isset($product['product_id']) && isset($product['price'])) {
            $check = Product::find($product['product_id']);
            if (isset($check->id) && is_numeric($product['price'])) {
                $check_item = Item::where('order_id', '=', $order->id)->where('product_id', '=', $check->id)->first();
                $quantity = 1;
                if (isset($product['quantity']) && is_numeric($product['quantity'])) {
                    $quantity = $product['quantity'];
                }
                if (isset($check_item->id)) {
                    $check_item->quantity = $check_item->quantity + $quantity;
                    $check_item->unit_price = $product['price'];
                    $check_item->save();
                } else {
                    $item = new Item;
                    $item->order_id = $order->id;
                    $item->product_id = $check->id;
                    $item->quantity = $quantity;
                    $item->unit_price = $product['price'];
                    $item->save();
                }

            } else {
                return ['message' => 'Product not found or incorrect price format'];
            }
        } else {
            return ['message' => 'Product format is incorrect. Please provide product ID & unit price.'];
        }
    }

    private function insertDiscountApi($discount, $order)
    {
        if (isset($discount['discount_id']) && isset($discount['priority'])) {
            $discount_table = Discount::find($discount['discount_id']);
            if (!isset($discount_table->id)) {
                return ['message' => 'Discount ID not found'];
            }
        } else if (isset($discount['type']) && isset($discount['value']) && isset($discount['priority'])) {
            $discount_table = Discount::where('type', '=', $discount['type'])
                ->where('value', '=', $discount['value'])->first();
            if (!isset($discount_table->id)) {
                $discount_table = new Discount;
                $discount_table->type = $discount['type'];
                $discount_table->value = $discount['value'];
                $discount_table->save();
            }
        } else {
            return ['message' => 'Discount format is incorrect.'];
        }
        $order_discount = new OrderDiscount;
        $order_discount->order_id = $order->id;
        $order_discount->discount_id = $discount_table->id;
        $order_discount->priority = $discount['priority'];
        $order_discount->save();
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        if (isset($order->id)) {
            if (isset($request->order_date)) {
                $order->order_date = date('Y-m-d H:i:s', strtotime($request->order_date));
            }

            if (isset($request->customer_id)) {
                $check = Customer::find($request->customer_id);
                if (isset($check->id)) {
                    $order->customer_id = $request->customer_id;
                } else {
                    return ['message' => 'Customer ID does not exist'];
                }

            }
            if (isset($request->shipping_price)) {
                $order->shipping_price = $request->shipping_price;
            }

            $order->save();

            if(strtolower($request->type) == "replace"){
                    $items = Item::where('order_id','=', $order->id)->get();
                    foreach($items as $val){
                        $val->delete();
                    }
                    $order_discounts = OrderDiscount::where('order_id','=', $order->id)->get();
                    foreach($order_discounts as $val){
                        $val->delete();
                    }
            }

            // insert items
            if (isset($request->products)) {
                foreach ($request->products as $i => $product) {
                    $item = $this->insertItemApi($product, $order);
                    if (isset($item['message'])) {
                        return $item;
                    }

                }
            }
            // insert discount
            if (isset($request->discounts)) {
                foreach ($request->discounts as $discount) {
                    $disc = $this->insertDiscountApi($discount, $order);
                    if (isset($disc['message'])) {
                        return $disc;
                    }

                }
            }

            return ['message' => 'Updated Successfully.', 'data' => Order::summary($order->id)];
        } else {
            return ['message' => 'Order does not exist'];
        }

    }

    public function delete($id)
    {
        $order = Order::find($id);
        if (isset($order->id)) {
            $order->delete();

            return ['message' => 'Deleted Successfully.'];
        } else {
            return ['message' => 'Order does not exist'];
        }
    }

}
